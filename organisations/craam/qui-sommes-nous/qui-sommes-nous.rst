
.. _qui_sommes_nous:

=================================================================================================================
Qui sommes nous
=================================================================================================================


- https://craam.noblogs.org/post/2022/01/01/qui-sommes-nous/


Basé en Auvergne-Rhône-Alpes, le Collectif Régional Anti-Armement et Militarisme
a pour objectif de mettre en lien les diverses initiatives anti-militaristes
de la région.

Dans un contexte où les tensions internationales et la répression des
mouvements sociaux partout dans le monde sont en hausse, où – conséquence
directe – l’industrie de l’armement est en plein boum, et où foisonnent
les politiques publiques militaristes – hausse des budgets, Service National Universel,
etc. –, nous considérons qu’il est urgent de développer un mouvement
anti-militariste d’ampleur.

Toutes les énergies sont les bienvenues, n’hésitez pas à nous contacter !

craam@riseup.net


Sites amis
=============


Observatoire des armements (Lyon)
------------------ ---------------

- https://www.obsarm.info/

Créé en 1984 à Lyon, sous le nom de Centre de documentation et de recherche
sur la paix et les conflits (CDRPC), l’Observatoire des armements a pour
objectif d’étayer les travaux de la société civile sur les questions de
défense et de sécurité et ce, dans la perspective d’une démilitarisation progressive.



ggrothendieck (Grenoble)
----------------------------------

- https://ggrothendieck.wordpress.com/

Le groupe Grothendieck est composé d’étudiants, de chômeurs, de démissionnaires
de l’université. Il publie régulièrement des textes, articles critiques
du techno-capitalisme et de la technoscience.

Il a déjà sorti un livre, L’Université désintégrée, la recherche grenobloise
au service du complexe militaro-industriel (Le monde à l’envers, 2020).


halteaucontrolenumerique (Saint-Etienne)
--------------------------------------------------

- https://halteaucontrolenumerique.fr/

Collectif stéphanois critique du tout-numérique.

Halte au contrôle numérique est un collectif stéphanois formé en 2019,
en opposition au projet Serenicity d'implantation de micros dans le mobilier
urbain d'un quartier populaire, et à l'installation d'un atelier Google
en centre-ville.

Ses actions prennent la forme de conférences, d'ateliers de sensibilisation
du grand public aux enjeux du tout-numérique, ou encore d'interpellations
des pouvoirs publics quant à certains projets.

Composé de citoyen-ne-s aux sensibilités diverses, son objectif est de
s'emparer des questions liées à la transformation numérique de nos sociétés,
afin d'ériger des garde-fous et de leur redonner une dimension politique
dont on cherche parfois à nous déposséder.


